extends Node

func _ready():
	randomize()
	var exit = self.get_children()[randi() % self.get_child_count()]
	exit.name = "Exit"
	
	var stairs = Sprite.new()
	stairs.texture = load("res://big-stairs-down.png")
	exit.add_child(stairs)
	
	var static_body = StaticBody2D.new()
	static_body.name = "Exit"
	exit.add_child(static_body)
	
	var rect = RectangleShape2D.new()
	rect.set_extents(Vector2(16, 16))
	
	var collision = CollisionShape2D.new()
	collision.set_shape(rect)
	static_body.add_child(collision)
