extends KinematicBody2D

const MOVE_SPEED = 1.25
var prevDirection = null
var spaceBarDown = false

func _ready():
	pass # Replace with function body.

func _physics_process(_delta):
	var move = Vector2(0, 0)
	var direction = null
	
	if Input.is_key_pressed(KEY_LEFT):
		move.x -= MOVE_SPEED
		direction = "left"
		if prevDirection != direction:
			$AnimatedSprite.animation = "Side Walk"
			$AnimatedSprite.flip_h = true
	if Input.is_key_pressed(KEY_RIGHT):
		move.x += MOVE_SPEED
		direction = "right"
		if prevDirection != direction:
			$AnimatedSprite.animation = "Side Walk"
			$AnimatedSprite.flip_h = false
	if Input.is_key_pressed(KEY_DOWN):
		move.y += MOVE_SPEED
		direction = "down"
		if prevDirection != direction:
			$AnimatedSprite.animation = "Front Walk"
			$AnimatedSprite.flip_h = false
	if Input.is_key_pressed(KEY_UP):
		move.y -= MOVE_SPEED
		direction = "up"
		if prevDirection != direction:
			$AnimatedSprite.animation = "Back Walk"
			$AnimatedSprite.flip_h = false
	
	if !direction && prevDirection:
		if prevDirection == "left":
			$AnimatedSprite.animation = "Side"
		if prevDirection == "right":
			$AnimatedSprite.animation = "Side"
		if prevDirection == "down":
			$AnimatedSprite.animation = "Front"
		if prevDirection == "up":
			$AnimatedSprite.animation = "Back"

	var collision = self.move_and_collide(move)
	
	if collision:
		if collision.collider.name == "Exit":
			get_tree().quit()
			
	prevDirection = direction
	
func _process(delta):
	if Input.is_key_pressed(KEY_SPACE):
		if !spaceBarDown:
			var lamp = Sprite.new()
			lamp.texture = load("res://lamp.png")
			lamp.position = self.position
			lamp.scale = Vector2(0.5, 0.5)
			get_parent().add_child(lamp)
		spaceBarDown = true
	else:
		spaceBarDown = false
		
