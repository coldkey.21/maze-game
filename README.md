# Maze Game

## Concept

**Description:** I have always enjoyed drawing mazes on graph paper, and it is time to learn a game engine and turn the drawings into a game. This will be a simple maze game where players are placed at the start of a maze and find their way to the end.

**Genre:** Puzzle

**Target Audience:** All Ages

**Time to develop:** 30 days

## Design

### Perspective
This game will be in 2D with the player looking from the top down


### Mechanics

* Movement: The player moves through the maze trying to find the end
* Start Point: Where the player starts the maze
* End Point: Where the player has won the level
* Walls: Areas that the player can not move through
* Symbol Placement (Bonus): The players can place a symbol to mark parts of the maze
    * Limit the number of symbols that can be placed and require the player to pick them up

### Game Engine

This game will be done using [Godot](https://godotengine.org/).

### Art Assets

This game will use [THE DUNGEON - TOP DOWN TILESET](https://www.gameart2d.com/the-dungeon---top-down-tileset.html) and [THE KNIGHTS - GAME SPRITES](https://www.gameart2d.com/the-knights---game-sprites.html).

### Sound Assets

#### Sounds Needed:
* Looping Game Track
* Movement
* Maze Complete

#### Looping Game Track
* Quality: Low
* Time: 1 minute
* Inspiration: Dungeon Synth

### Milestones

#### Week

1. Learn Godot, see [Godot Tutorials](https://conceptartempire.com/godot-tutorials/)
1. Create basic mechanics
    * Movement
    * Start Point
    * End Point
    * Walls
1. Create first level, intro screen, and Credits screen
1. Add art and sound

#### Bonus Time
* Create Symbol Placement mechanic
* Make extra levels